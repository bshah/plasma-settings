# Plasma Settings

Settings application for Plasma Mobile.

## API

You can find documentation for using the Active Settings infrastructure at
[docs.plasma-mobile.org/PlasmaSettings.html](https://docs.plasma-mobile.org/PlasmaSettings.html)
